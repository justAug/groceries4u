package com.example.myfirstapp;

import java.util.ArrayList;

public class OrderMarketPlaceObject {


    String dateRequested;
    int priority;
    int productID;
    int quantity;
    String requesterID;
    String supplierID;
    String dateSupplied;

    String productName;
    String unitOfProduct;

    long dateRequestedInSecs;

    public String getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(String dateRequested) {
        this.dateRequested = dateRequested;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getRequesterID() {
        return requesterID;
    }

    public void setRequesterID(String requesterID) {
        this.requesterID = requesterID;
    }

    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    public String getDateSupplied() {
        return dateSupplied;
    }

    public void setDateSupplied(String dateSupplied) {
        this.dateSupplied = dateSupplied;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUnitOfProduct() {
        return unitOfProduct;
    }

    public void setUnitOfProduct(String unitOfProduct) {
        this.unitOfProduct = unitOfProduct;
    }

    public long getDateRequestedInSecs() {
        return dateRequestedInSecs;
    }

    public void setDateRequestedInSecs(long dateRequestedInSecs) {
        this.dateRequestedInSecs = dateRequestedInSecs;
    }
}