package com.example.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class EntryPage extends AppCompatActivity {
    //initial page that the user sees when they open the app. Directs them to where they want to go

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_page);

    }

    public void loginDirect(View view)
    {
        Intent loginIntent = new Intent(EntryPage.this, LoginPage.class);
        loginIntent.putExtra("Jeff", "Jeff");
        startActivity(loginIntent);
    }

    public void regDirect(View view)
    {
        Intent regIntent = new Intent(EntryPage.this, RegisterPage.class);
        startActivity(regIntent);
    }

    public void newIntentTime(View view){
        Intent testIntent = new Intent(EntryPage.this, shoppingList.class);
        startActivity(testIntent);
    }

    public void newIntentTime2(View view){
        Intent testIntent = new Intent(EntryPage.this, ViewShoppingList.class);
        startActivity(testIntent);
    }
}