package com.example.myfirstapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class UserProfile extends AppCompatActivity {

    TextView usernameDisp, emailDisp, nameDisp, addressDisp, supplierDisp, requesterDisp;
    DatabaseReference userRef;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        // code for navigation view

        bottomNavigationView = findViewById(R.id.bottomNavigation);
        BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        System.out.println("In bottom navigation code");
                        switch (item.getItemId()) {
                            case R.id.marketPlace:
                                System.out.println("Go to marketplace");
                                startActivity(new Intent(UserProfile.this, OrderMarketPlace.class ));
                                return true;
                            case R.id.addItem:
                                startActivity(new Intent( UserProfile.this, shoppingList.class ));
                                return true;
                            case R.id.viewShoppingList:
                                startActivity(new Intent(UserProfile.this, ViewShoppingList.class ));
                                return true;
                            case R.id.profile:
//                                startActivity(new Intent(UserProfile.this, UserProfile.class ));
                                return true;
                        }
                        return false;
                    }
                };
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        bottomNavigationView.setSelectedItemId(R.id.profile);

        // ends here

        usernameDisp = (TextView)findViewById(R.id.usernameDisplay);
        emailDisp = (TextView)findViewById(R.id.emailDisplay);
        nameDisp  = (TextView)findViewById(R.id.nameDisplay);
        addressDisp  = (TextView)findViewById(R.id.addressDisplay);
        supplierDisp = (TextView)findViewById(R.id.supplierDisplay);
        requesterDisp = (TextView)findViewById(R.id.requesterDisplay);

    }

    public void getDetails(View v)
    {
        String currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();

        userRef = FirebaseDatabase.getInstance().getReference().child("userInfo").child(currentUser);
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                String username = snapshot.child("username").getValue().toString();
                String email = snapshot.child("email").getValue().toString();
                String fName = snapshot.child("fName").getValue().toString();
                String mName = snapshot.child("mName").getValue().toString();
                String lName = snapshot.child("lName").getValue().toString();
                String address1 = snapshot.child("address1").getValue().toString();
                String address2 = snapshot.child("address2").getValue().toString();
                String supplier = snapshot.child("supplier").getValue().toString();
                String requester = snapshot.child("requester").getValue().toString();

                String name = "";
                if(mName.isEmpty()){
                    name = fName + " " + lName;
                }
                else{
                    name = fName + " " + mName + " " + lName;
                }

                String address = "";
                if(address2.isEmpty()){
                    address = address1;
                }
                else{
                    address = address1 + ", \n" + address2;
                }

                usernameDisp.setText(username);
                emailDisp.setText(email);
                nameDisp.setText(name);
                addressDisp.setText(address);
                supplierDisp.setText(supplier);
                requesterDisp.setText(requester);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }
}