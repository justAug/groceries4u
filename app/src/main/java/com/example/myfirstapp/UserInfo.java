package com.example.myfirstapp;

public class UserInfo {

    private String fName = "";
    private String mName = "";
    private String lName = "";
    private String email = "";
    private String username = "";
    private String address1 = "";
    private String address2 = "";
    private Boolean supplier = false;
    private Boolean requester = false;

    public UserInfo() {
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        System.out.println("Say my name");
        this.fName = fName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public Boolean getSupplier() {
        return supplier;
    }

    public void setSupplier(Boolean supplier) {
        this.supplier = supplier;
    }

    public Boolean getRequester() {
        return requester;
    }

    public void setRequester(Boolean requester) {
        this.requester = requester;
    }
}
