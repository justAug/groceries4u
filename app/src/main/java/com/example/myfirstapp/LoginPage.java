package com.example.myfirstapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginPage extends AppCompatActivity {
    /*
    This class is used to handle all of the login activity with the app. It lets the user enter
    their credentials and then checks those against the authentication side of the FireBase RealTime
    database. If true, it lets you login. Else it throws the error for the user
     */

    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        Intent intent = new Intent(LoginPage.this, EntryPage.class);
        Bundle bundle = getIntent().getExtras();
        String result = bundle.getString("Jeff");
        // that was me just messing about
        System.out.println(result);

    }

    public void loginFunction(View v)
    {
        mAuth = FirebaseAuth.getInstance();

        final View view = v;

        EditText email = (EditText)findViewById(R.id.loginEmail);
        EditText password = (EditText)findViewById(R.id.loginPassword);

        String loginEmail = email.getText().toString();
        String loginPassword = password.getText().toString();

        System.out.println("Login email : " + loginEmail);
        System.out.println("Login password : " + loginPassword);
        //use the inbuilt sign in function provided to check credentials with the database
        mAuth.signInWithEmailAndPassword(loginEmail, loginPassword)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                           sucessLogin();
                        } else {
                            // If sign in fails, display a message to the user.
                            Snackbar.make(view, task.getException().getMessage(), Snackbar.LENGTH_LONG).show();
                        }
                    }
                });


    }

    // move from log in page to home page
    public void sucessLogin()
    {
        Intent loginConfirmed = new Intent(LoginPage.this, AccountCreated.class);
        String result = "Necessary details entered";
        loginConfirmed.putExtra("result", result);
        startActivity(loginConfirmed);
    }

}