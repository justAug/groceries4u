package com.example.myfirstapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.security.cert.CollectionCertStoreParameters;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

public class OrderMarketPlace extends AppCompatActivity {

    LinearLayout marketPlaceBox;
    ArrayList<String> dateRequestedList;
    ArrayList<Integer> priorityList;
    ArrayList<Integer> productIDList;
    ArrayList<Integer> quantityList;
    ArrayList<String> requesterIDList;
    ArrayList<String> supplierIDList;
    ArrayList<String> dateSuppliedList;

    ArrayList<String> productNameList;
    ArrayList<String> unitOfProduct;
    ArrayList<Integer> productIDsFromNameQuery;

    ArrayList<Long> dateRequestedInSecsList;

    ArrayList<OrderMarketPlaceObject> objList;

    String currentUser;

    BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_market_place);

        // code for navigation view

        bottomNavigationView = findViewById(R.id.bottomNavigation);
        BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        System.out.println("In bottom navigation code");
                        switch (item.getItemId()) {
                            case R.id.marketPlace:
                                System.out.println("Go to marketplace");
//                                startActivity(new Intent(shoppingList.this, OrderMarketPlace.class ));
                                return true;
                            case R.id.addItem:
                                startActivity(new Intent( OrderMarketPlace.this, shoppingList.class ));
                                return true;
                            case R.id.viewShoppingList:
                                startActivity(new Intent(OrderMarketPlace.this, ViewShoppingList.class ));
                                return true;
                            case R.id.profile:
                                startActivity(new Intent(OrderMarketPlace.this, UserProfile.class ));
                                return true;
                        }
                        return false;
                    }
                };
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        bottomNavigationView.setSelectedItemId(R.id.marketPlace);

        // ends here


        //   currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
        // To be used later when integrated with login page

        currentUser =  "TcmtkRThgwWLRQqoJQIGE6nUAol2";

        marketPlaceBox = (LinearLayout)findViewById(R.id.marketPlaceBox);

        getDataFromOrderTable();

        setUpGUI();
    }

    @SuppressLint("ResourceType")
    public void setUpGUI()
    {
        TextView mainHeader = new TextView(this);
        mainHeader.setText("MarketPlace");
        mainHeader.setTextSize(18);
        mainHeader.setTextColor(Color.BLACK);
        mainHeader.setGravity(Gravity.CENTER);
        mainHeader.setId(1);
        mainHeader.setTag("MarketPlaceHeader");
        mainHeader.setPadding(10, 10, 10, 10);
        mainHeader.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        marketPlaceBox.addView(mainHeader);
    }

    public void updateGUI()
    {
        System.out.println("In updateGui");
        for(int i = 0; i <  productIDList.size(); i++) {
            // checks to make sure you dont display any orders that belong to the user or orders that have already been taken by other users
            System.out.println("for " + i + " in updateGui");
//            if(currentUser.equals(requesterIDList.get(i)))
            System.out.println("hey before getting supplier id ");
            System.out.println(objList.get(i).getSupplierID());
            System.out.println("after getting supplier id");
            if(currentUser.equals(objList.get(i).getRequesterID()))
                {
                    System.out.println("Cant select your own order");
                    continue;
                }
//            else if(supplierIDList.get(i) != "")
            else if(!objList.get(i).getSupplierID().equals(""))
               {
                   System.out.println("Cant select something already vouched for");
                   continue;
               }

            LinearLayout boxLine1 = new LinearLayout(this);
            boxLine1.setOrientation(LinearLayout.HORIZONTAL);

            // add a picture of the product here

            TextView productName = new TextView(this);
//            productName.setText(productNameList.get(i));
            productName.setText(objList.get(i).getProductName());
            productName.setTextSize(18);
            productName.setTextColor(Color.BLACK);
            productName.setGravity(Gravity.CENTER);
            productName.setId(100 + i);
            productName.setTag("prodName" + i);
            productName.setPadding(10, 10, 10, 10);
            productName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 2.0f));
            boxLine1.addView(productName);

            final TextView quant = new TextView(this);
//            quant.setText(quantityList.get(i) + " " + unitOfProduct.get(i));
            quant.setText(objList.get(i).getQuantity() + " " +  objList.get(i).getUnitOfProduct());
            quant.setTextSize(18);
            quant.setTextColor(Color.BLACK);
            quant.setGravity(Gravity.CENTER);
            quant.setId(300 + i);
            quant.setTag("quantity" + i);
            quant.setPadding(10, 10, 10, 10);
            quant.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            boxLine1.addView(quant);

            marketPlaceBox.addView(boxLine1);

            Button addToMyList = new Button(this);
            addToMyList.setText("Add this item to your list?");
            addToMyList.setId(500 + i);
            addToMyList.setTag("Button" + i);
            addToMyList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String tag = (String) view.getTag();
                    int index = Integer.parseInt(tag.substring(6)); // length of the word button is 6

                    addSupplierToOrder(index);
                }
            });

            marketPlaceBox.addView(addToMyList);
        }
    }

    public void getDataFromOrderTable()
    {
        DatabaseReference orderList = FirebaseDatabase.getInstance().getReference().child("Orders");

        dateRequestedList = new ArrayList<>();
        priorityList = new ArrayList<>();
        productIDList = new ArrayList<>();
        quantityList = new ArrayList<>();
        dateSuppliedList = new ArrayList<>();
        supplierIDList = new ArrayList<>();
        requesterIDList = new ArrayList<>();
        productNameList = new ArrayList<>();

        objList = new ArrayList<>();

        final int[] counter = {0};

        orderList.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot issue : snapshot.getChildren()) {
                        OrderMarketPlaceObject obj = new OrderMarketPlaceObject();

                        obj.setDateRequested(issue.child("dateRequested").getValue().toString());
                        obj.setPriority(Integer.parseInt(issue.child("priority").getValue().toString()));
//                        obj.setProductID(Integer.parseInt(issue.child("productId").getValue().toString()));
                        obj.setQuantity(Integer.parseInt(issue.child("quantity").getValue().toString()));
                        obj.setDateSupplied(issue.child("dateSupplied").getValue().toString());
                        obj.setSupplierID(issue.child("supplierId").getValue().toString());
                        obj.setRequesterID(issue.child("requesterId").getValue().toString());

                        objList.add(obj);
//
//
                        dateRequestedList.add(issue.child("dateRequested").getValue().toString());
//
//                        priorityList.add(Integer.parseInt(issue.child("priority").getValue().toString()));
                        productIDList.add(Integer.parseInt(issue.child("productId").getValue().toString()));
//                        quantityList.add(Integer.parseInt(issue.child("quantity").getValue().toString()));
//
//                        dateSuppliedList.add(issue.child("dateSupplied").getValue().toString());
//                        supplierIDList.add(issue.child("supplierId").getValue().toString());
//                        requesterIDList.add(issue.child("requesterId").getValue().toString());

                        counter[0]++;
                        // do a second query on the product list to get the name of the product

                        getProductName(counter[0]);

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void getProductName(final int counter) {
        System.out.println("in get product name");
        for(int a : productIDList)
        {
            System.out.println(a);
        }
        final int productID = productIDList.get(counter - 1);
//        final int productID = objList.get(counter - 1).getProductID();
        System.out.println("productID before query = " + productID);
        Query query2 = FirebaseDatabase.getInstance().getReference().child("1JjZbhNSiOGPxiiwfGch29JGW2cVCa6Vd4O7pYrPzYNI").child("Sheet1")
                .orderByChild("productID").equalTo(productID);
        final String[] productName = {""};
        unitOfProduct = new ArrayList<>();
        productIDsFromNameQuery = new ArrayList<Integer>();
        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        System.out.println("productID = " + productID);
                        productIDsFromNameQuery.add(productID);
                        productName[0] = (issue.child("Name").getValue().toString());
                        System.out.println(productName[0]);
                        productNameList.add(productName[0]);

                        System.out.println(productNameList.size());
                        String temp = issue.child("Unit").getValue().toString();
                        unitOfProduct.add(temp);
                    }
                }
                if(productIDsFromNameQuery.size() == objList.size())
                {

                    makingListsMatch();
                    getRequesterTimeInSecs();
                    System.out.println("Info on array list");
                    System.out.println(productNameList.size());
//                    System.out.println(quantityList.size());
                    for(int i = 0; i < productNameList.size(); i++){
                        System.out.println(productNameList.get(i));
//                        System.out.println(quantityList.get(i));
                        System.out.println(unitOfProduct.get(i));
                    }

                    updateGUI();
                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void makingListsMatch()
    {
        System.out.println(productIDList.size());
        System.out.println(productIDsFromNameQuery.size());
        System.out.println(objList.size());
        for(int i = 0; i < productIDList.size(); i++)
        {
            if(productIDList.get(i) == productIDsFromNameQuery.get(i))
            {
                objList.get(i).setProductName(productNameList.get(i));
                objList.get(i).setUnitOfProduct(unitOfProduct.get(i));
//                continue;
            }
            else
            {
                int j = productIDList.indexOf(productIDsFromNameQuery.get(i));
                Collections.swap(productIDList, i, j);
                Collections.swap(quantityList, i, j);

                Collections.swap(objList, i, j);

                objList.get(i).setProductName(productNameList.get(i));
                objList.get(i).setUnitOfProduct(unitOfProduct.get(i));
            }
        }
    }

    public void getRequesterTimeInSecs()
    {
        LocalDateTime ldt = LocalDateTime.now();
        dateRequestedInSecsList = new ArrayList<>();

        System.out.println(dateRequestedList.size());
        for(int i = 0 ; i < dateRequestedList.size(); i++)
        {

            int reqDay = Integer.parseInt(dateRequestedList.get(i).substring(0, 2));
            int reqMonth = Integer.parseInt(dateRequestedList.get(i).substring(3, 5));
            int reqYear = Integer.parseInt(dateRequestedList.get(i).substring(6, 10));
            int reqSecs = Integer.parseInt(dateRequestedList.get(i).substring(11));

            int reqHour = reqSecs / (60 * 60);
            int reqMins = (reqSecs - (60 * 60 * reqHour)) / 60;
            reqSecs = reqSecs - (60 * 60 * reqHour) - (reqMins * 60);

            LocalDateTime currentTime = LocalDateTime.of(reqYear, reqMonth, reqDay,
                    reqHour, reqMins, reqSecs);


//            long years = ldt.until(currentTime, ChronoUnit.YEARS);
//            tempDateTime = tempDateTime.plusYears(years);
//
//            long months = ldt.until(currentTime, ChronoUnit.MONTHS);
//            tempDateTime = tempDateTime.plusMonths(months);
//
//            long days = ldt.until(currentTime, ChronoUnit.DAYS);
//            tempDateTime = tempDateTime.plusDays(days);
//
//
//            long hours = ldt.until(currentTime, ChronoUnit.HOURS);
//            tempDateTime = tempDateTime.plusHours(hours);
//
//            long minutes = ldt.until(currentTime, ChronoUnit.MINUTES);
//            tempDateTime = tempDateTime.plusMinutes(minutes);

            long seconds = ldt.until(currentTime, ChronoUnit.SECONDS);
            System.out.println("time  " + seconds);

            dateRequestedInSecsList.add(seconds);
            System.out.println(dateRequestedInSecsList.size());
            objList.get(i).setDateRequestedInSecs(seconds);

        }
        orderByTime();
    }

    public void orderByTime()
    {
// all times are in GMT
//        String dateRequested = "";
//        LocalDateTime ldt = LocalDateTime.now();
//        int dayOfMonth = ldt.getDayOfMonth();
//        int monthOfYear = ldt.getMonthValue();
//        int year = ldt.getYear();
//
//        System.out.println(dayOfMonth);
//        System.out.println(monthOfYear);
//        System.out.println(year);
//
//        if(dayOfMonth < 10)
//        {
//            dateRequested += "0" + dayOfMonth + "-";
//        }
//        else
//        {
//            dateRequested += dayOfMonth + "-";
//        }
//        if(monthOfYear < 10)
//        {
//            dateRequested += "0" + monthOfYear + "-";
//        }
//        else
//        {
//            dateRequested += monthOfYear;
//        }
//        dateRequested += year + "-";
//
//        int time = 0;
//        int sec = ldt.getSecond();
//        int minute = ldt.getMinute();
//        int hour = ldt.getHour();
//        time = sec + (60 * minute )+ (60 * 60 * hour);
//        System.out.println(time);
//
//        dateRequested += time;

        for(int i =0; i < objList.size(); i++)
        {
            System.out.println("in date requested list loop");
            for(int j=1; j < ( objList.size()-i); j++){
                if( dateRequestedInSecsList.get(j-1) >  dateRequestedInSecsList.get(j))
                {
                    //swap elements
                    System.out.println(dateRequestedInSecsList.get(j-1));
                    long temp = dateRequestedInSecsList.get(j-1);
                    dateRequestedInSecsList.set(j-1, dateRequestedInSecsList.get(j));
                    dateRequestedInSecsList.set(j, temp);

                    Collections.swap(objList, j , (j - 1));
                }
            }
        }
        System.out.println("order by time stuff ");
        for(int i = 0 ;  i <  objList.size(); i++)
        {
            System.out.println(dateRequestedInSecsList);
            System.out.println(objList.get(i).getProductName());
        }
    }



    public void addSupplierToOrder(final int index)
    {
        final placeOrderInterface inter  = new placeOrderInterface();

        final String childNeeded = productIDList.get(index) + "-" + requesterIDList.get(index);
        Query query = FirebaseDatabase.getInstance().getReference().child("Orders").child(childNeeded);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                System.out.println(snapshot.exists());
                if(snapshot.exists()){
                    for(DataSnapshot issue: snapshot.getChildren())
                    {
                        inter.setRequesterId(requesterIDList.get(index));
                        inter.setProductId(productIDList.get(index));
                        inter.setQuantity(quantityList.get(index));
                        inter.setPriority(priorityList.get(index));
                        inter.setDateRequested(dateRequestedList.get(index));
                        inter.setSupplierId(currentUser);

                        FirebaseDatabase.getInstance().getReference().child(("Orders")).child(childNeeded).setValue(inter);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}