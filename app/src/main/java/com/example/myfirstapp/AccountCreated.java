package com.example.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class AccountCreated extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_created);
        Intent intent = new Intent(AccountCreated.this, LoginPage.class);
        Bundle bundle = getIntent().getExtras();
        String result = bundle.getString("result");
        System.out.println(result);
        TextView textView = (TextView) findViewById(R.id.accountConfirmed);
        textView.setText(result);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(AccountCreated.this, HomePage.class);
                startActivity(intent);
                finish();
            }
        }, 4000);
    }
}
