package com.example.myfirstapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class shoppingList extends AppCompatActivity implements DecideQuantityFragment.QuantityDecided {

    DatabaseReference reference;

    Spinner catSpinner;
    ArrayList<String> categoriesFromDB;
    String category;

    Spinner prodSpinner;
    ArrayList<String> productsFromDB;
    String product;
    int productID;

    Spinner quantitySpinner;
    String quantityUnit;
    double quantity;
    ArrayList<String> quantitysFromDB;

    AddToBasketInterface basketInterface;
    long maxID = 0;
    DatabaseReference sendToBasket;
    String currentUser;
    int basketID;
    DataSnapshot dupeProduct;
    View view;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);

        // code for navigation view

        bottomNavigationView = findViewById(R.id.bottomNavigation);
        BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        System.out.println("In bottom navigation code");
                        switch (item.getItemId()) {
                            case R.id.marketPlace:
                                System.out.println("Go to marketplace");
                                startActivity(new Intent(shoppingList.this, OrderMarketPlace.class ));
                                return true;
                            case R.id.addItem:
//                                startActivity(new Intent(shoppingList.this, OrderMarketPlace.class ));
                                return true;
                            case R.id.viewShoppingList:
                                startActivity(new Intent(shoppingList.this, ViewShoppingList.class ));
                                return true;
                            case R.id.profile:
                                startActivity(new Intent(shoppingList.this, UserProfile.class ));
                                return true;
                        }
                        return false;
                    }
                };
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        bottomNavigationView.setSelectedItemId(R.id.addItem);
        // ends here



        System.out.println("About to get data reference");
        reference = FirebaseDatabase.getInstance().getReference().child("1JjZbhNSiOGPxiiwfGch29JGW2cVCa6Vd4O7pYrPzYNI").child("Sheet1");
        System.out.println("Got data refernce");

        //        currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
        // To be used later when integrated with login page

        currentUser =  "axFcp4dvNufFPj6DOq0YH38uBge2";

        catSpinner = (Spinner)findViewById(R.id.categoryList);
        categoriesFromDB = new ArrayList<>();

        populateCategories();

        prodSpinner = (Spinner)findViewById(R.id.productNameList);
        productsFromDB = new ArrayList<>();

        quantitySpinner = (Spinner)findViewById(R.id.unitSpinner);
        quantitysFromDB = new ArrayList<>();

        sendToBasket = FirebaseDatabase.getInstance().getReference().child("Basket");
        sendToBasket.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    maxID = (snapshot.getChildrenCount());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }

    public void populateCategories()
    // method used to get the categories from the database and add them to spinner
    {
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        String name = issue.child("Category").getValue().toString();
                        if (categoriesFromDB.contains(name)) {
                            System.out.println("name is in list");
                        } else {
                            System.out.println("Name is not in list. Adding " + name);
                            categoriesFromDB.add(name);
                        }
                    }
                    System.out.println("Just exited loop. Is categories empty : " + categoriesFromDB.isEmpty());
                    for (String item : categoriesFromDB) {
                        System.out.println(item);
                    }
                }
                makeCatSpinner(); // must be done sequentially because database calls are asynchronous
            }

            @Override
            public void onCancelled (@NonNull DatabaseError error){

            }
        });
    }

    public void makeCatSpinner()
    // use the list of catgeories retrieved to populate the spinner
    {
        System.out.println("in make cat spinner");
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, categoriesFromDB);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        catSpinner.setAdapter(adapter);
        catSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("Click on category dropdown");
                category = (String) adapterView.getItemAtPosition(i);

                System.out.println("Category chosen : " + category);
                populateProducts();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void populateProducts()
    // method used to get the products from the database and add them to spinner based on the
    // category chosen
    {
        Query query = reference.orderByChild("Category").equalTo(category);
        productsFromDB = new ArrayList<>();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.exists()) {
                for (DataSnapshot issue : dataSnapshot.getChildren()) {
                    productsFromDB.add(issue.child("Name").getValue().toString()); //  add all products of that category to the drop down list
                }
            }
            makeProdSpinner();
        }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void makeProdSpinner()
    // use the products retrieved from the database and make the spinner for the products
    {
        System.out.println("in make prod spinner");
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, productsFromDB);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        prodSpinner.setAdapter(adapter);
        prodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("Click on product dropdown");
                product = (String) adapterView.getItemAtPosition(i);

                System.out.println("Product chosen : " + product);
                populateQuantitySpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    public void populateQuantitySpinner()
    // get the quantity unit used for the product selected
    {
        Query query = reference.orderByChild("Name").equalTo(product);
        final String[] quantity = {""};
        productID = 0;
        quantitysFromDB =new ArrayList<>();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        quantity[0] = (issue.child("Unit").getValue().toString()); //  add all products of that category to the drop down list
                        String temp = issue.child("productID").getValue().toString();
                        productID = Integer.parseInt(temp);
                    }
                }
                quantitysFromDB.add(quantity[0]);
                if(quantity[0].equals("kg"))
                {
                    quantitysFromDB.add("g");
                }
                else if(quantity[0].equals("litre"))
                {
                    quantitysFromDB.add("ml");
                }
                makeQuantitySpinner();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    public void makeQuantitySpinner()
    // populate the spinner based on the unit such as using smaller units for kg and l
    {
        System.out.println("in make quantity spinner");
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, quantitysFromDB);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        quantitySpinner.setAdapter(adapter);
        quantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("Click on quantity dropdown");
                quantityUnit = (String) adapterView.getItemAtPosition(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void addToBasket(View view)
    // on clicking the add to basket button, it starts doing all the checks before sending to
    // checkInBasket
    {
        closeKeyboard(view);

        EditText quantityVal = (EditText)findViewById(R.id.quantityValue);
        String temp = quantityVal.getText().toString();
        if(!temp.isEmpty()) {
            quantity = Double.parseDouble(temp);

            System.out.println("quantity chosen : " + quantityUnit);
            if (quantityUnit.equals("g") || quantityUnit.equals("ml")) {
                quantity /= 1000;
            }
            System.out.print("Quantity is " +  quantity + " " + quantitysFromDB.get(0));
        }

        System.out.println("________________________\n\n\n\n");
        System.out.println("Category : " + category);
        System.out.println("Product : " + product);
        System.out.println("Quantity : " + quantity + " " + quantitysFromDB.get(0));
        System.out.println("Product ID " + productID);

        if(quantity == 0)
        {
            Snackbar.make(view, "Need to enter a valid quantity", Snackbar.LENGTH_LONG).show();
            ColorStateList colorStateList = ColorStateList.valueOf(Color.RED);
            ViewCompat.setBackgroundTintList(quantityVal, colorStateList);
        }
        else
        {
            ColorStateList colorStateList = ColorStateList.valueOf(Color.GREEN);
            ViewCompat.setBackgroundTintList(quantityVal, colorStateList);
            checkIfProductInBasket(view);

        }

    }

    public void viewBasketDirect(View view)
    {

        Intent intent = new Intent(shoppingList.this, ViewBasket.class);
        System.out.println("Going to view basket");
        startActivity(intent);

    }

    public void checkIfProductInBasket(final View view)
   // if in the basket, then create a dialog asking the user what they want to do. else go to
    // putting stuff in basket
    {
        DatabaseReference basket = FirebaseDatabase.getInstance().getReference().child("Basket");
        Query check = basket.orderByChild("reference").equalTo(productID + "-" + currentUser);
        final double[] quant = {0.0};
        this.view = view;
        basketID = 0;
        check.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                System.out.println(snapshot.exists());
                if(snapshot.exists())
                {
                    for (DataSnapshot issue : snapshot.getChildren()){

                        // add asking whether they want more units adding to what is in basket or not.
                        System.out.println(productID + "-" + currentUser);
                        quant[0] = Double.parseDouble(issue.child("quantity").getValue().toString());
                        System.out.println("Quant[0]  = " + quant[0]);
                        basketID = Integer.parseInt(issue.child("basketID").getValue().toString());
                        dupeProduct = issue;
                        buildAlertDialog(quant[0]);
                    }
                }
                else
                {
                    putItemInBasket(quant[0], view);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void putItemInBasket(Double oldQuant, View view)
    {

        basketInterface = new AddToBasketInterface();

//        String currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
        // To be used later when integrated with login page

        String currentUser =  "axFcp4dvNufFPj6DOq0YH38uBge2";
        basketInterface.setUserID(currentUser);
        basketInterface.setProductID(productID);
        basketInterface.setQuantity(quantity);
        basketInterface.setReference(String.valueOf(productID) + "-" + currentUser);

        if(oldQuant != 0.0)
        {
            System.out.println("Found a duplicate in basket and adding more stuff to it");
            sendToBasket.child((String.valueOf(productID) + "-" + currentUser)).setValue(basketInterface);
        }

        else {
            System.out.println("New product timeee");
            sendToBasket.child((String.valueOf(productID) + "-" + currentUser)).setValue(basketInterface);

        }

        Snackbar.make(view, "This item has been added to your basket", Snackbar.LENGTH_LONG).show();


    }

    public void buildAlertDialog(Double oldQuantity)
    {
        DecideQuantityFragment frag = new DecideQuantityFragment(oldQuantity, quantity, product, quantityUnit);
        frag.show(getSupportFragmentManager(), "quantityDecider");
    }

    public void quantitiesDecision(Double finalQuantity)
    // based on what the user decides, you delete the old entry replace it with a new entry with
    // the new quantity value
    {
        final Double oldQuantity = this.quantity;
        System.out.println("Old quantity before query  = " + oldQuantity);
        DatabaseReference basket = FirebaseDatabase.getInstance().getReference().child("Basket");
        Query check = basket.orderByChild("reference").equalTo(productID + "-" + currentUser);
            quantity = finalQuantity;

            check.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    System.out.println("Inside query for removing old copy");
                    System.out.println(snapshot.exists());
                    if(snapshot.exists())
                    {
                        for (DataSnapshot issue : snapshot.getChildren()){
                            issue.getRef().removeValue();
                            putItemInBasket(oldQuantity, view);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

    }


    public void closeKeyboard(View view)
    {
        if(view != null)
        {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void setFinalQuantity(Double finalquantity) {
        quantitiesDecision(finalquantity);
    }
}
