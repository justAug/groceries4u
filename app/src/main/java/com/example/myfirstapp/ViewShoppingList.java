package com.example.myfirstapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

public class ViewShoppingList extends AppCompatActivity {

    LinearLayout container;
    BottomNavigationView bottomNavigationView;
    String currentUser;

    ArrayList<String> dateRequestedList;
    ArrayList<Integer> priorityList;
    ArrayList<Integer> productIDList;
    ArrayList<Integer> quantityList;
    ArrayList<String> dateSuppliedList;
    ArrayList<String> supplierIDList;
    ArrayList<String> requesterIDList;

    ArrayList<Long> dateRequestedInSecsList;

    ArrayList<String> productNameList;
    ArrayList<String> unitOfProduct;
    ArrayList<Integer> productIDsFromNameQuery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_shopping_list);


        bottomNavigationView = findViewById(R.id.bottomNavigation);
        BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        System.out.println("In bottom navigation code");
                        switch (item.getItemId()) {
                            case R.id.marketPlace:
                                System.out.println("Go to marketplace");
                                startActivity(new Intent(ViewShoppingList.this, OrderMarketPlace.class ));
                                return true;
                            case R.id.addItem:
                                startActivity(new Intent( ViewShoppingList.this, shoppingList.class ));
                                return true;
                            case R.id.viewShoppingList:
//                                startActivity(new Intent(ViewShoppingList.this, ViewShoppingList.class ));
                                return true;
                            case R.id.profile:
                                startActivity(new Intent(ViewShoppingList.this, UserProfile.class ));
                                return true;
                        }
                        return false;
                    }
                };
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        bottomNavigationView.setSelectedItemId(R.id.viewShoppingList);

        // ends here

        container = (LinearLayout)findViewById(R.id.viewShoppingListContainer);

        //   currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
        // To be used later when integrated with login page

        currentUser =  "axFcp4dvNufFPj6DOq0YH38uBge2";

        setUpGUI();

        getDataFromOrderTable();

    }

    @SuppressLint("ResourceType")
    public void setUpGUI()
    {
        TextView mainHeader = new TextView(this);
        mainHeader.setText("Your Shopping List");
        mainHeader.setTextSize(18);
        mainHeader.setTextColor(Color.BLACK);
        mainHeader.setGravity(Gravity.CENTER);
        mainHeader.setId(1);
        mainHeader.setTag("MarketPlaceHeader");
        mainHeader.setPadding(10, 10, 10, 10);
        mainHeader.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        container.addView(mainHeader);
    }

    public void updateGUI()
    {
        for(int i = 0; i <  productIDList.size(); i++) {

            LinearLayout boxLine1 = new LinearLayout(this);
            boxLine1.setOrientation(LinearLayout.HORIZONTAL);

            // add a picture of the product here

            TextView productName = new TextView(this);
            productName.setText(productNameList.get(i));
            productName.setTextSize(18);
            productName.setTextColor(Color.BLACK);
            productName.setGravity(Gravity.CENTER);
            productName.setId(100 + i);
            productName.setTag("prodName" + i);
            productName.setPadding(10, 10, 10, 10);
            productName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 2.0f));
            boxLine1.addView(productName);

            final TextView quant = new TextView(this);
            quant.setText(quantityList.get(i) + " " + unitOfProduct.get(i));
            quant.setTextSize(18);
            quant.setTextColor(Color.BLACK);
            quant.setGravity(Gravity.CENTER);
            quant.setId(300 + i);
            quant.setTag("quantity" + i);
            quant.setPadding(10, 10, 10, 10);
            quant.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            boxLine1.addView(quant);

            container.addView(boxLine1);

            Button removeFromMyList = new Button(this);
            removeFromMyList.setText("Remove this item from your list?");
            removeFromMyList.setId(500 + i);
            removeFromMyList.setTag("Button" + i);
            removeFromMyList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String tag = (String) view.getTag();
                    int index = Integer.parseInt(tag.substring(6)); // length of the word button is 6

                    removeSupplierFromOrder(index);
                }
            });

            container.addView(removeFromMyList);
        }
    }

    public void getDataFromOrderTable()
    {
        DatabaseReference orderList = FirebaseDatabase.getInstance().getReference().child("Orders");

        dateRequestedList = new ArrayList<>();
        priorityList = new ArrayList<>();
        productIDList = new ArrayList<>();
        quantityList = new ArrayList<>();
        dateSuppliedList = new ArrayList<>();
        supplierIDList = new ArrayList<>();
        requesterIDList = new ArrayList<>();
        productNameList = new ArrayList<>();

        final int[] counter = {0};

        Query query = orderList.orderByChild("supplierId").equalTo(currentUser);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot issue : snapshot.getChildren()) {
                        dateRequestedList.add(issue.child("dateRequested").getValue().toString());

                        priorityList.add(Integer.parseInt(issue.child("priority").getValue().toString()));
                        productIDList.add(Integer.parseInt(issue.child("productId").getValue().toString()));
                        quantityList.add(Integer.parseInt(issue.child("quantity").getValue().toString()));

                        dateSuppliedList.add(issue.child("dateSupplied").getValue().toString());
                        supplierIDList.add(issue.child("requesterId").getValue().toString());
                        requesterIDList.add(issue.child("requesterId").getValue().toString());

                        counter[0]++;
                        // do a second query on the product list to get the name of the product

                        getProductName(counter[0]);

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void getProductName(final int counter) {
        System.out.println("in get product name");
        for(int a : productIDList)
        {
            System.out.println(a);
        }
        final int productID = productIDList.get(counter - 1);
        System.out.println("productID before query = " + productID);
        Query query2 = FirebaseDatabase.getInstance().getReference().child("1JjZbhNSiOGPxiiwfGch29JGW2cVCa6Vd4O7pYrPzYNI").child("Sheet1")
                .orderByChild("productID").equalTo(productID);
        final String[] productName = {""};
        unitOfProduct = new ArrayList<>();
        productIDsFromNameQuery = new ArrayList<Integer>();
        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        System.out.println("productID = " + productID);
                        productIDsFromNameQuery.add(productID);
                        productName[0] = (issue.child("Name").getValue().toString());
                        System.out.println(productName[0]);
                        productNameList.add(productName[0]);
                        System.out.println(productNameList.size());
                        String temp = issue.child("Unit").getValue().toString();
                        unitOfProduct.add(temp);
                    }
                }
                if(productNameList.size() == quantityList.size())
                {
                    makingListsMatch();
                    System.out.println("Info on array list");
                    System.out.println(productNameList.size());
                    System.out.println(quantityList.size());
                    for(int i = 0; i < productNameList.size(); i++){
                        System.out.println(productNameList.get(i));
                        System.out.println(quantityList.get(i));
                        System.out.println(unitOfProduct.get(i));
                    }

                    updateGUI();
                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void makingListsMatch()
    {
        for(int i = 0; i < productIDList.size(); i++)
        {
            if(productIDList.get(i) == productIDsFromNameQuery.get(i))
            {
                continue;
            }
            else
            {
                int j = productIDList.indexOf(productIDsFromNameQuery.get(i));
                Collections.swap(productIDList, i, j);
                Collections.swap(quantityList, i, j);
            }
        }
    }

    public void removeSupplierFromOrder(final int index)
    {
        final placeOrderInterface inter  = new placeOrderInterface();

        final String childNeeded = productIDList.get(index) + "-" + requesterIDList.get(index);
        Query query = FirebaseDatabase.getInstance().getReference().child("Orders").child(childNeeded);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                System.out.println(snapshot.exists());
                if(snapshot.exists()){
                    for(DataSnapshot issue: snapshot.getChildren())
                    {
                        inter.setRequesterId(requesterIDList.get(index));
                        inter.setProductId(productIDList.get(index));
                        inter.setQuantity(quantityList.get(index));
                        inter.setPriority(priorityList.get(index));
                        inter.setDateRequested(dateRequestedList.get(index));

                        FirebaseDatabase.getInstance().getReference().child(("Orders")).child(childNeeded).setValue(inter);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
