package com.example.myfirstapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.hardware.input.InputManager;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterPage extends AppCompatActivity {
    /*
    This class handles when a new user wants to register to be able to use the app. It takes the
    users important details and sends them to the database which is used to make an account for them
    and also make a child node in the users collection in the database
     */
    private FirebaseAuth mAuth;

    String fName ;
    String mName;
    String lName;
    String email;
    String username;
    String password;
    String address1;
    String address2;

    Boolean supply = false;

    Boolean request = false;

    UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);
    }

    public void setIncomplete(EditText edit){ // method used for setting showing that field is incomplete in the ui by changing colour of underline to red
        ColorStateList colorStateList = ColorStateList.valueOf(Color.RED);
        ViewCompat.setBackgroundTintList(edit, colorStateList);
    }

    public void setComplete(EditText edit) {// method used for setting showing that field is complete in the ui by changing colour of underline to green
        ColorStateList colorStateList = ColorStateList.valueOf(Color.GREEN);
        ViewCompat.setBackgroundTintList(edit, colorStateList);
    }

    public void registerAcc(String email, String password, View v) // method to check whether user can register with their current credentials
    {
        final View view = v;
        mAuth = FirebaseAuth.getInstance();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() { // Firebase method to create users for the app given an email and password
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                           successRegister("Account Registered Successfully");
                        }
                        else {
                            Snackbar.make(view, task.getException().getMessage(), Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void successRegister(String resultFinal) // method to direct to the next page and will be used to add all of users details to DB
    {
        System.out.println(resultFinal);
        System.out.println(fName);

        userInfo = new UserInfo();

        String currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference sendUserInfo = database.getReference().child("userInfo");
//      used to make an object that is sent to the database to set up the user account
        userInfo.setfName(fName);
        userInfo.setmName(mName);
        userInfo.setlName(lName);
        userInfo.setEmail(email);
        userInfo.setUsername(username);
        userInfo.setAddress1(address1);
        userInfo.setAddress2(address2);
        userInfo.setSupplier(supply);
        userInfo.setRequester(request);

        System.out.println(userInfo.getEmail());

        sendUserInfo.child(currentUser).setValue(userInfo);

//        Intent intent = new Intent(RegisterPage.this, AccountCreated.class);
//        intent.putExtra("result", resultFinal);

        Intent intent = new Intent(RegisterPage.this, shoppingList.class);
        System.out.println("going to shopping");
        startActivity(intent); // directing user to next page

    }

    public void sendMessage(View view) { // called when the send button on register page is pressed

        EditText edit1 = (EditText)findViewById(R.id.enterMessage1);
        EditText edit2 = (EditText)findViewById(R.id.enterMessage2);
        EditText edit3 = (EditText)findViewById(R.id.enterMessage3);
        EditText edit4 = (EditText)findViewById(R.id.enterMessage4);
        EditText edit5 = (EditText)findViewById(R.id.enterMessage5);
        EditText edit6 = (EditText)findViewById(R.id.enterMessage6);
        EditText edit7 = (EditText)findViewById(R.id.enterMessage7);
        EditText edit8 = (EditText)findViewById(R.id.enterMessage8);

        // get responses from all fields and convert them into usable forms

        fName = edit1.getText().toString();
        mName = edit2.getText().toString();
        lName = edit3.getText().toString();
        email = edit4.getText().toString();
        username = edit5.getText().toString();
        password = edit6.getText().toString();
        address1 = edit7.getText().toString();
        address2 = edit8.getText().toString();

        CheckBox supplier = (CheckBox)findViewById(R.id.supplierBox);
        supply = supplier.isChecked();

        CheckBox requester  = (CheckBox)findViewById(R.id.requesterBox);
        request = requester.isChecked();

        String result = "";

        // do checks on each required field to see if completed correctly

        if(fName.equals(""))
        {
            result = "Incomplete details";
            setIncomplete(edit1);
        }
        else
        {
            setComplete(edit1);
        }

        if(lName.equals(""))
        {
            result = "Incomplete details";
            setIncomplete(edit3);
        }
        else
        {
            setComplete(edit3);
        }

        String emailRegex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);

        if(!matcher.matches()) // add checks for whether email has been used
        {
            result = "Incomplete details";
            setIncomplete(edit4);
        }
        else
        {
            setComplete(edit4);
        }

        if(username.equals(""))  // add checks for if the user name is taken
        {
            result = "Incomplete details";
            setIncomplete(edit5);
        }
        else
        {
            setComplete(edit5);
        }

        if(password.equals(""))
        {
            result = "Incomplete details";
            setIncomplete(edit6);
        }
        else
        {
            setComplete(edit6);
        }

        if(address1.equals(""))
        {
            result = "Incomplete details";
            setIncomplete(edit7);
        }
        else
        {
            setComplete(edit7);
        }


        if(!supply && !request)
        {
            result = "One of the boxes must be ticked";
            supplier.setBackgroundColor(Color.YELLOW);
            requester.setBackgroundColor(Color.YELLOW);
        }
        else
        {
            supplier.setBackgroundColor(Color.WHITE);
            requester.setBackgroundColor(Color.WHITE);
        }

    if(result.isEmpty())
        registerAcc(email, password, view);
    else
        Snackbar.make(view, result, Snackbar.LENGTH_LONG).show();
    }

    // method to close the keyboard when checkbox is selected
    public void closeKeyboard(View view)
    {
        if(view != null)
        {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}