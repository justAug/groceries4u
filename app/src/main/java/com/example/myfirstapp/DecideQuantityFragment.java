package com.example.myfirstapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class DecideQuantityFragment extends DialogFragment {
    /*
    This class is used to make the popup box on the shopping list class. The popup is used in a
    situation where the product that the user wants to add to the basket, is already in the
    basket. If so, it lets the user decide whether they want to keep the old quantity, replace the
    quantity in the basket with the one chosen, or to add the two quantities together.

     */

    private double oldQuantity;
    private double newQuantity;
    private String productName;
    private String unitOfProduct;

    private QuantityDecided listener;

    //constructor used when insitialising the object in the shoppinglist class
    public DecideQuantityFragment(double oldQuantity, double newQuantity, String productName, String unitOfProduct)
    {
        this.oldQuantity = oldQuantity;
        this.newQuantity = newQuantity;
        this.productName = productName;
        this.unitOfProduct = unitOfProduct;
    }


    public interface QuantityDecided{
        void setFinalQuantity(Double finalQuantity);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (QuantityDecided) context;
        }catch(Exception e){
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        // method used to make the popup with the relevant details and the buttons to choose which
        // action they want to choose

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String message = "You have " + oldQuantity + " " + unitOfProduct + " of " + productName
                + " in your basket. Would you like to add " + newQuantity + " " + unitOfProduct
                + " more to the basket, change the quantity to " + newQuantity + " "
                + unitOfProduct + " or leave the quantity as " + oldQuantity + " " + unitOfProduct;
        builder.setMessage(message);

        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.setFinalQuantity((oldQuantity + newQuantity));
            }
        });

        builder.setNegativeButton("Replace", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.setFinalQuantity(newQuantity);
            }
        });

        builder.setNeutralButton("Keep original", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.setFinalQuantity(oldQuantity);
            }
        });

        return builder.create();
    }
}
