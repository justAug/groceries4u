package com.example.myfirstapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

public class ViewBasket extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    ArrayList<String> productsInBasket;
    ArrayList<Integer> productIDs;
    ArrayList<Integer> productIDsFromNameQuery;
    ArrayList<Double> quantitiesInBasket;
    ArrayList<String> unitOfProduct;
    ArrayList<TextView> quantityValues;
    DatabaseReference basketReference;
    DatabaseReference productsReference;
    LinearLayout table;
    String currentUser;
    int itemSelected = -2; // set as a value that it cannot be if it was an actual index

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_basket);


        // code for navigation view

        bottomNavigationView = findViewById(R.id.bottomNavigation);
        BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        System.out.println("In bottom navigation code");
                        switch (item.getItemId()) {
                            case R.id.marketPlace:
                                System.out.println("Go to marketplace");
                                startActivity(new Intent(ViewBasket.this, OrderMarketPlace.class ));
                                return true;
                            case R.id.addItem:
                                startActivity(new Intent( ViewBasket.this, shoppingList.class ));
                                return true;
                            case R.id.viewShoppingList:
                                startActivity(new Intent(ViewBasket.this, ViewShoppingList.class ));
                                return true;
                            case R.id.profile:
                                startActivity(new Intent(ViewBasket.this, UserProfile.class ));
                                return true;
                        }
                        return false;
                    }
                };
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

//        bottomNavigationView.setSelectedItemId(R.id.addItem);
        // ends here

        getData();

        setupGUI();
    }

    public void setupGUI()
    {
        table = (LinearLayout) findViewById(R.id.tableContainer);
        System.out.println("Number of children in table : " + table.getChildCount());

        LinearLayout tableHeader = new LinearLayout(this);
        tableHeader.setOrientation(LinearLayout.HORIZONTAL);
        tableHeader.setId(0);

        TextView header = new TextView(this);
        header.setText("My Basket");
        header.setId(1);
        header.setPadding(20, 20,20,80);
        header.setTextSize(30);
        header.setTextColor(Color.BLACK);
        header.setGravity(Gravity.CENTER);
        header.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));

        tableHeader.addView(header);

        table.addView(tableHeader);

    }
    public void getData()
    {
        // String currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
        // To be used later when integrated with login page
        System.out.println("In getData");

        currentUser =  "axFcp4dvNufFPj6DOq0YH38uBge2";

        productsReference = FirebaseDatabase.getInstance().getReference().child("1JjZbhNSiOGPxiiwfGch29JGW2cVCa6Vd4O7pYrPzYNI").child("Sheet1");

        basketReference = FirebaseDatabase.getInstance().getReference().child("Basket");

        Query query = basketReference.orderByChild("userID").equalTo(currentUser);

        productsInBasket = new ArrayList<>();
        quantitiesInBasket = new ArrayList<>();
        productIDs = new ArrayList<>();

        final int[] counter = {0};
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        String temp = issue.child("productID").getValue().toString();
                        System.out.println(temp);
                        int productID = Integer.parseInt(temp);
                        productIDs.add(productID);
                        counter[0]++;
                        getProductName(productID, counter[0]);
                        quantitiesInBasket.add(Double.parseDouble(issue.child("quantity").getValue().toString()));
                    }
                }
                else{
                    TextView emptyBasket = new TextView(ViewBasket.this);
                    emptyBasket.setText("You have no items in your basket");
                    emptyBasket.setId(20);
                    emptyBasket.setPadding(20, 20,20,80);
                    emptyBasket.setTextSize(30);
                    emptyBasket.setTextColor(Color.BLACK);
                    emptyBasket.setGravity(Gravity.CENTER);
                    emptyBasket.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));


                    table.addView(emptyBasket);
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void getProductName(final int productID2, final int counter) {
        System.out.println("in get product name");
        for(int a : productIDs)
        {
            System.out.println(a);
        }
        final int productID = productIDs.get(counter - 1);
        System.out.println("productID before query = " + productID);
        Query query2 = productsReference.orderByChild("productID").equalTo(productID);
        final String[] productName = {""};
        unitOfProduct = new ArrayList<>();
        productIDsFromNameQuery = new ArrayList<>();
        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        System.out.println("productID = " + productID);
                        productIDsFromNameQuery.add(productID);
                        productName[0] = (issue.child("Name").getValue().toString());
                        System.out.println(productName[0]);
                        productsInBasket.add(productName[0]);
                        System.out.println(productsInBasket.size());
                        String temp = issue.child("Unit").getValue().toString();
                        unitOfProduct.add(temp);
                    }
                }
                if(productsInBasket.size() == quantitiesInBasket.size())
                {
                    makingListsMatch();
                    System.out.println("Info on array list");
                    System.out.println(productsInBasket.size());
                    System.out.println(quantitiesInBasket.size());
                    for(int i = 0; i < productsInBasket.size(); i++){
                        System.out.println(productsInBasket.get(i));
                        System.out.println(quantitiesInBasket.get(i));
                        System.out.println(unitOfProduct.get(i));
                    }

                    updateGUI();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void updateGUI(){
        quantityValues = new ArrayList<>();
        for(int i = 0; i < productsInBasket.size(); i++) {
            LinearLayout row = new LinearLayout(this);
            row.setOrientation(LinearLayout.HORIZONTAL);
            TextView name = new TextView(this);
            name.setText(productsInBasket.get(i));
            name.setTextSize(18);
            name.setTextColor(Color.BLACK);
            name.setGravity(Gravity.CENTER);
            name.setId(100 + i);
            name.setTag("prodName" + i);
            name.setPadding(10, 10, 10, 10);
            name.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 2.0f));
            row.addView(name);

            final TextView quant = new TextView(this);
            quant.setText(quantitiesInBasket.get(i) + " " + unitOfProduct.get(i));
            quant.setTextSize(18);
            quant.setTextColor(Color.BLACK);
            quant.setGravity(Gravity.CENTER);
            quant.setId(300 + i);
            quant.setTag("quantity" + i);
            quant.setPadding(10, 10, 10, 10);
            quant.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            quantityValues.add(quant);
            row.addView(quant);

            System.out.println("itemSelcted = " + itemSelected);
            System.out.println("i" + i);

            if(itemSelected != -2 && itemSelected == i) // When an item is selected, then there are + and - buttons to increase or decrease the quantity by one.
            {
                LinearLayout buttons = new LinearLayout(this);
                buttons.setOrientation(LinearLayout.VERTICAL);

                Button increaseQuant = new Button(this);
                increaseQuant.setText("+");
                increaseQuant.setId(700 + i);
                increaseQuant.setTag("UpQuant");
                final int finalI = i;
                increaseQuant.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println(finalI);
//                        TextView quantityValue = (TextView)view.findViewWithTag("quantity" + finalI);
                        TextView quantityValue = quantityValues.get(finalI);
                        String temp = quantityValue.getText().toString();
                        int point = temp.indexOf(".");
                        int space = temp.indexOf(" ");
                        Double first = Double.parseDouble(temp.substring(0, point));
                        Double second = Double.parseDouble(temp.substring(point+1, space));
                        int length = String.valueOf(second).length();
                        second = second / (Math.pow(10, length));
                        Double value = first + second;
//                        int value = Integer.parseInt(quantityValue.getText().toString());
                        value++;
                        quantityValue.setText(String.valueOf(value) + " " + unitOfProduct.get(finalI));
                    }
                });

                Button decreaseQuant = new Button(this);
                decreaseQuant.setText("-");
                decreaseQuant.setId(900 + i);
                decreaseQuant.setTag("DownQuant");
                final int finalI2 = i;
                decreaseQuant.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        TextView quantityValue = (TextView)view.findViewWithTag("quantity" + finalI2);
                        TextView quantityValue = quantityValues.get(finalI);
                        String temp = quantityValue.getText().toString();
                        int point = temp.indexOf(".");
                        int space = temp.indexOf(" ");
                        Double first = Double.parseDouble(temp.substring(0, point));
                        Double second = Double.parseDouble(temp.substring(point+1, space));
                        int length = String.valueOf(second).length();
                        second = second / (Math.pow(10, length));
                        Double value = first + second;
//                        int value = Integer.parseInt(quantityValue.getText().toString());
                        System.out.println("Value = " + value);
                        if(value > 0)
                        {
                            value--;
                        }
                        else
                        {
//                            Snackbar.make(ViewBasket.this, "Cannot make quantity less than 0", Snackbar.LENGTH_LONG);
                            Toast.makeText(ViewBasket.this,  "Cannot make quantity less than 0", Toast.LENGTH_LONG).show();
                        }
                        quantityValue.setText(String.valueOf(value) + " " + unitOfProduct.get(finalI));
                    }
                });

                buttons.addView(increaseQuant);
                buttons.addView(decreaseQuant);

                row.addView(buttons);
            }

            Button editProd = new Button(this);
            editProd.setText("Edit");
            editProd.setId(500 + i);
            editProd.setTag("button" + i);
            editProd.setPadding(10, 10, 10, 10);
            editProd.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    // when click on edit button, you get options of removing product, or changing quantity of product
                    System.out.println(v.getTag());
                    String btnTag = (String) v.getTag();
                    //length of word button is 6. So the temp variable stores the index of the product as the tag is button + number
                    String temp =   btnTag.substring(6);;
                    itemSelected = Integer.parseInt(temp);
                    popupMenu(v);
                }
            });
            editProd.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));

            row.addView(editProd);

            table.addView(row);

            if(itemSelected != -2  && itemSelected == i)
            {
                Button saveQuantity = new Button(this);
                saveQuantity.setText("Save");
                saveQuantity.setPadding(10, 40, 10, 10);
                saveQuantity.setGravity(Gravity.CENTER);
                saveQuantity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        TextView quantityValue = (TextView)view.findViewWithTag("quantity" + itemSelected);
//                        int value = Integer.parseInt(quantityValue.getText().toString());
                        TextView quantityValue = quantityValues.get(itemSelected);
                        String temp = quantityValue.getText().toString();
                        int point = temp.indexOf(".");
                        int space = temp.indexOf(" ");
                        Double first = Double.parseDouble(temp.substring(0, point));
                        Double second = Double.parseDouble(temp.substring(point+1, space));
                        int length = String.valueOf(second).length();
                        second = second / (Math.pow(10, length));
                        Double value = first + second;
                        if(value == 0)
                        {
                            removeProduct();
                            Snackbar.make(getCurrentFocus(), "The product has been removed", Snackbar.LENGTH_LONG);
                        }
                        else
                        {
                            // save the quantity to the basket and then update the gui
                            AddToBasketInterface basketInterface = new AddToBasketInterface();
                            basketInterface.setUserID(currentUser);
                            basketInterface.setQuantity(value);
                            basketInterface.setProductID(productIDs.get(itemSelected));
                            basketInterface.setReference((productIDs.get(itemSelected)) + "-" + currentUser);

                            DatabaseReference sendToBasket = FirebaseDatabase.getInstance().getReference().child("Basket");

                            sendToBasket.child((productIDs.get(itemSelected)) + "-" + currentUser).setValue(basketInterface);
                            Toast.makeText(ViewBasket.this, " The quantity of the product has been changed", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                table.addView(saveQuantity);
            }
        }

        LinearLayout buttonSpace = new LinearLayout(this);
        buttonSpace.setOrientation(LinearLayout.HORIZONTAL);
//        buttonSpace.setGravity(Gravity.BOTTOM | Gravity.CENTER);
        buttonSpace.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));

        Button backToShopping = new Button(this);
        backToShopping.setText("More shopping to do?");
        backToShopping.setGravity(Gravity.CENTER);
        backToShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(ViewBasket.this, shoppingList.class);
                startActivity(intent);
            }
        });

        Button onToCheckout = new Button(this);
        onToCheckout.setText("On to Checkout");
        onToCheckout.setGravity(Gravity.CENTER);
        onToCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(ViewBasket.this, PlaceOrder.class);
                startActivity(intent);
            }
        });

        buttonSpace.addView(backToShopping,
                new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0));

        buttonSpace.addView(onToCheckout,
                new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        0));

        table.addView(buttonSpace);



    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch(menuItem.getItemId()){
            case R.id.viewBasketQuantityBtn: System.out.println("Quantity");
            changeQuantity();
            return true;
            case R.id.viewBasketRemoveBtn:System.out.println("Remove");
            removeProduct();
            return true;
            default:System.out.println("Default");
            return false;

        }
    }

    public void popupMenu(View v)
    {
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.inflate(R.menu.view_basket_popup_menu);
        popupMenu.show();
    }

    public void removeProduct()
    {

        DatabaseReference basket = FirebaseDatabase.getInstance().getReference().child("Basket");
        Query check = basket.orderByChild("reference").equalTo(productIDs.get(itemSelected) + "-" + currentUser);
        //Gimmicky sorta fix. Needs work
//        Query check = basket.orderByChild("reference").equalTo(productIDsFromNameQuery.get(itemSelected) + "-" + currentUser);
//        System.out.println((itemSelected + "-" + currentUser).equals("1-axFcp4dvNufFPj6DOq0YH38uBge2"));
        check.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                System.out.println(snapshot.exists());
                if(snapshot.exists())
                {
                    for(DataSnapshot issue: snapshot.getChildren())
                    {
                        issue.getRef().removeValue();
//                        productsInBasket.remove(itemSelected);
//                        quantitiesInBasket.remove(itemSelected);
//                        unitOfProduct.remove(itemSelected);
//                        table.removeAllViews();
//                        setupGUI();
//                        updateGUI();
                        Intent intent = new Intent(ViewBasket.this, ViewBasket.class);
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void makingListsMatch()
    {
        for(int i = 0; i < productIDs.size(); i++)
        {
            if(productIDs.get(i) == productIDsFromNameQuery.get(i))
            {
                continue;
            }
            else
            {
                int j = productIDs.indexOf(productIDsFromNameQuery.get(i));
                Collections.swap(productIDs, i, j);
                Collections.swap(quantitiesInBasket, i, j);
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(ViewBasket.this, shoppingList.class);
        startActivity(intent);
    }

    public void changeQuantity()
    {
        table.removeAllViews();
        setupGUI();
        updateGUI();
    }
}