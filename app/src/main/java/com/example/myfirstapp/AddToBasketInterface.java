package com.example.myfirstapp;

public class AddToBasketInterface {
    /*
    This class is used as an interface for the shoppingList class.
    It is used to make an object for the data to go into the basket on the firebase realtime database.
    It is mainly used for the setters.

     */

    private int productID;
    private double quantity;
    private String userID;
    private String reference;

    public AddToBasketInterface(){

    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public double getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

}
