package com.example.myfirstapp;

import java.util.Date;
import java.time.LocalDateTime;

public class placeOrderInterface {

    private String requesterId;
    private String supplierId;
    private int productId;
    private double quantity;
    private int priority;
    private String dateRequested;
    private String dateSupplied;

    public placeOrderInterface()
    {
        supplierId = "";
        dateSupplied = "";
//        makeRequesterDate();
    }


    public String getRequesterId() {
        return requesterId;
    }

    public void setRequesterId(String requesterId) {
        this.requesterId = requesterId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(String dateRequested) {
        this.dateRequested = dateRequested;
    }

    public String getDateSupplied() {
        return dateSupplied;
    }

    public void setDateSupplied(String dateSupplied) {
        this.dateSupplied = dateSupplied;
    }

    public void makeRequesterDate()
    {
        // all times are in GMT
        dateRequested = "";
        LocalDateTime ldt = LocalDateTime.now();
        int dayOfMonth = ldt.getDayOfMonth();
        int monthOfYear = ldt.getMonthValue();
        int year = ldt.getYear();
        System.out.println(dayOfMonth);
        System.out.println(monthOfYear);
        System.out.println(year);

        if(dayOfMonth < 10)
        {
            dateRequested += "0" + dayOfMonth + "-";
        }
        else
        {
            dateRequested += dayOfMonth + "-";
        }
        if(monthOfYear < 10)
        {
            dateRequested += "0" + monthOfYear + "-";
        }
        else
        {
            dateRequested += monthOfYear;
        }
        dateRequested += year + "-";

        int time = 0;
        int sec = ldt.getSecond();
        int minute = ldt.getMinute();
        int hour = ldt.getHour();
        time = sec + (60 * minute )+ (60 * 60 * hour);
        System.out.println(time);

        dateRequested += time;


    }

}
