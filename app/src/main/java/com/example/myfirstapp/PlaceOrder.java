package com.example.myfirstapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

// only go to order class if there is something in basket. ELSE do not show the button to go to the basket
public class PlaceOrder extends AppCompatActivity {


    ArrayList<String> productsInBasket;
    ArrayList<Integer> productIDs;
    ArrayList<Integer> productIDsFromNameQuery;
    ArrayList<Double> quantitiesInBasket;
    ArrayList<String> unitOfProduct;
    ArrayList<TextView> quantityValues;
    ArrayList<Integer> priorityProduct;
    DatabaseReference basketReference;
    DatabaseReference productsReference;
    String currentUser;
    LinearLayout checkoutBox;
    Map<Integer, Integer> priorityList;

    BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);

        // code for navigation view

        bottomNavigationView = findViewById(R.id.bottomNavigation);
        BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        System.out.println("In bottom navigation code");
                        switch (item.getItemId()) {
                            case R.id.marketPlace:
                                System.out.println("Go to marketplace");
                                startActivity(new Intent(PlaceOrder.this, OrderMarketPlace.class ));
                                return true;
                            case R.id.addItem:
                                startActivity(new Intent( PlaceOrder.this, shoppingList.class ));
                                return true;
                            case R.id.viewShoppingList:
                                startActivity(new Intent(PlaceOrder.this, ViewShoppingList.class ));
                                return true;
                            case R.id.profile:
                                startActivity(new Intent(PlaceOrder.this, UserProfile.class ));
                                return true;
                        }
                        return false;
                    }
                };
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

//        bottomNavigationView.setSelectedItemId(R.id.addItem);

        // ends here

        getData();

        setUpGUI();

        priorityList = new HashMap<>();
    }

    public void setUpGUI()
    {
        checkoutBox = (LinearLayout)findViewById(R.id.CheckoutBox);

        TextView introMessage = new TextView(this);
        introMessage.setText("You have the following items in your basket");
        introMessage.setId(1);
        introMessage.setPadding(20, 20,20,80);
        introMessage.setTextSize(30);
        introMessage.setTextColor(Color.BLACK);
        introMessage.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        checkoutBox.addView(introMessage);

        LinearLayout tableRowHeader = new LinearLayout(this);
        tableRowHeader.setOrientation(LinearLayout.HORIZONTAL);
        tableRowHeader.setId(0);

        TextView productHeader = new TextView(this);
        productHeader.setText("Product Name");
        productHeader.setId(2);
        productHeader.setPadding(20, 20,20,80);
        productHeader.setTextSize(30);
        productHeader.setTextColor(Color.BLACK);
        productHeader.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 2.0f));

        TextView quantityHeader = new TextView(this);
        quantityHeader.setText("Quantity");
        quantityHeader.setId(3);
        quantityHeader.setPadding(20, 20,20,80);
        quantityHeader.setTextSize(30);
        quantityHeader.setTextColor(Color.BLACK);
        quantityHeader.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));

        TextView priorityHeader = new TextView(this);
        priorityHeader.setText("Priority");
        priorityHeader.setId(4);
        priorityHeader.setPadding(20, 20,20,80);
        priorityHeader.setTextSize(30);
        priorityHeader.setTextColor(Color.BLACK);
        priorityHeader.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));

        tableRowHeader.addView(productHeader);
        tableRowHeader.addView(quantityHeader);
        tableRowHeader.addView(priorityHeader);

        checkoutBox.addView(tableRowHeader);

    }

    public void getData()
    {
        // String currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
        // To be used later when integrated with login page
        System.out.println("In getData");

        currentUser =  "axFcp4dvNufFPj6DOq0YH38uBge2";

        productsReference = FirebaseDatabase.getInstance().getReference().child("1JjZbhNSiOGPxiiwfGch29JGW2cVCa6Vd4O7pYrPzYNI").child("Sheet1");

        basketReference = FirebaseDatabase.getInstance().getReference().child("Basket");

        Query query = basketReference.orderByChild("userID").equalTo(currentUser);

        productsInBasket = new ArrayList<>();
        quantitiesInBasket = new ArrayList<>();
        productIDs = new ArrayList<>();

        final int[] counter = {0};
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        String temp = issue.child("productID").getValue().toString();
                        System.out.println(temp);
                        int productID = Integer.parseInt(temp);
                        productIDs.add(productID);
                        counter[0]++;
                        getProductName(productID, counter[0]);
                        quantitiesInBasket.add(Double.parseDouble(issue.child("quantity").getValue().toString()));
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void getProductName(final int productID2, final int counter) {
        System.out.println("in get product name");
        final int productID = productIDs.get(counter - 1);
        Query query2 = productsReference.orderByChild("productID").equalTo(productID);
        final String[] productName = {""};
        unitOfProduct = new ArrayList<>();
        productIDsFromNameQuery = new ArrayList<>();
        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        productIDsFromNameQuery.add(productID);
                        productName[0] = (issue.child("Name").getValue().toString());
                        productsInBasket.add(productName[0]);
                        String temp = issue.child("Unit").getValue().toString();
                        unitOfProduct.add(temp);
                    }
                }
                if(productsInBasket.size() == quantitiesInBasket.size())
                {
                    makingListsMatch();
                    updateGUI();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void updateGUI() {
        quantityValues = new ArrayList<>();
        for (int i = 0; i < productsInBasket.size(); i++) {
            LinearLayout row = new LinearLayout(this);
            row.setOrientation(LinearLayout.HORIZONTAL);
            TextView name = new TextView(this);
            name.setText(productsInBasket.get(i));
            name.setTextSize(18);
            name.setTextColor(Color.BLACK);
            name.setGravity(Gravity.CENTER);
            name.setId(100 + i);
            name.setTag("prodName" + i);
            name.setPadding(10, 10, 10, 10);
            name.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 2.0f));
            row.addView(name);

            final TextView quant = new TextView(this);
            quant.setText(quantitiesInBasket.get(i) + " " + unitOfProduct.get(i));
            quant.setTextSize(18);
            quant.setTextColor(Color.BLACK);
            quant.setGravity(Gravity.CENTER);
            quant.setId(300 + i);
            quant.setTag("quantity" + i);
            quant.setPadding(10, 10, 10, 10);
            quant.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            quantityValues.add(quant);
            row.addView(quant);

            final Spinner prioritySpinner = new Spinner(this);
            prioritySpinner.setTag("priority" + i);
            prioritySpinner.setId(500 + i);

            System.out.println("Before on selected, tag  = " + prioritySpinner.getTag());

// Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.priorityOptions, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
            prioritySpinner.setAdapter(adapter);

            prioritySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    System.out.println("Click on category dropdown ");
                    System.out.println(Integer.parseInt((String) adapterView.getItemAtPosition(i)));
                    String temp = (String) adapterView.getTag();
                    System.out.println(temp);
                    int id = Integer.parseInt(temp.substring(8));

//                    priorityProduct.add(Integer.parseInt((String) adapterView.getItemAtPosition(i)));

                    priorityList.put(id, Integer.parseInt((String) adapterView.getItemAtPosition(i)));
//                    System.out.println("Category chosen : " + category);
//                    populateProducts();

                    for (Integer name: priorityList.keySet()){
                        String key = name.toString();
                        String value = priorityList.get(name).toString();
                        System.out.println(key + " " + value);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            row.addView(prioritySpinner);

            checkoutBox.addView(row);

        }
        LinearLayout orderBtnSpace = new LinearLayout(this);
        orderBtnSpace.setOrientation(LinearLayout.VERTICAL);
        orderBtnSpace.setGravity(Gravity.BOTTOM);
        orderBtnSpace.setId(10);

        Button orderBtn = new Button(this);
        orderBtn.setText("Order");
        orderBtn.setPadding(10, 10, 10, 10);
        orderBtn.setGravity(Gravity.CENTER);

        orderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeOrder();
            }
        });

        orderBtnSpace.addView(orderBtn);

        checkoutBox.addView(orderBtnSpace);

    }

    public void makingListsMatch()
    {
        for(int i = 0; i < productIDs.size(); i++)
        {
            if(productIDs.get(i) == productIDsFromNameQuery.get(i))
            {
                continue;
            }
            else
            {
                int j = productIDs.indexOf(productIDsFromNameQuery.get(i));
                Collections.swap(productIDs, i, j);
                Collections.swap(quantitiesInBasket, i, j);
            }
        }
    }

    public void placeOrder()
    {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Orders");

       // String currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
        // To be used later when integrated with login page

        currentUser =  "axFcp4dvNufFPj6DOq0YH38uBge2";
        Date date = new Date();

        String requesterDate = makeRequesterDate();

        // add all the items to the order list from the basket
        for(int i = 0; i < productIDs.size(); i++)
        {
            placeOrderInterface inter = new placeOrderInterface();

            inter.setRequesterId(currentUser);
            inter.setProductId(productIDs.get(i));
            inter.setQuantity(quantitiesInBasket.get(i));
            inter.setPriority(priorityList.get(i));
            inter.setDateRequested(requesterDate);

            reference.child((String.valueOf(productIDs.get(i)) + "-" + currentUser)).setValue(inter);
        }

        // once all items are added to the orderList, then remove them from the basket

        DatabaseReference removeFromBasket = FirebaseDatabase.getInstance().getReference().child("Basket");
        Query query = removeFromBasket.orderByChild("userID").equalTo(currentUser);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                System.out.println(snapshot.exists());
                if(snapshot.exists()){
                    for(DataSnapshot issue: snapshot.getChildren())
                    {
                        issue.getRef().removeValue();
                    }

                    Toast.makeText(PlaceOrder.this, "Your items have been ordered", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        // when done, make an intent from this page to the "home page"



    }

    public String makeRequesterDate()
    {
        // all times are in GMT
         String dateRequested = "";
        LocalDateTime ldt = LocalDateTime.now();
        int dayOfMonth = ldt.getDayOfMonth();
        int monthOfYear = ldt.getMonthValue();
        int year = ldt.getYear();
        System.out.println(dayOfMonth);
        System.out.println(monthOfYear);
        System.out.println(year);

        if(dayOfMonth < 10)
        {
            dateRequested += "0" + dayOfMonth + "-";
        }
        else
        {
            dateRequested += dayOfMonth + "-";
        }
        if(monthOfYear < 10)
        {
            dateRequested += "0" + monthOfYear + "-";
        }
        else
        {
            dateRequested += monthOfYear;
        }
        dateRequested += year + "-";

        int time = 0;
        int sec = ldt.getSecond();
        int minute = ldt.getMinute();
        int hour = ldt.getHour();
        time = sec + (60 * minute )+ (60 * 60 * hour);
        System.out.println(time);

        dateRequested += time;

        return dateRequested;


    }

}